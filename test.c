/* Any copyright is dedicated to the Public Domain.
 * https://creativecommons.org/publicdomain/zero/1.0/ */

#include "star_vm.h"

#include <stdio.h>

#include "yyy_test.h"

/*****************************************************************************/
/* Begin with a test for most opcodes. These are just smoke tests to ensure
 * don't fully crash, and can dispatch properly by testing the following
 * instructions being { NOP,TRAP } and { STOP }.
 */
/*****************************************************************************/
/* A few basic NOP/return tests */
static const unsigned char test_nop[] = {
    STAR_VM_NOP,
    STAR_VM_STOP
};

static int test_nop1(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    
    STAR_VM_INIT(&vm);
    
    /* Since this doesn't end in STOP, we expect a segfault. */
    YYY_EXPECT_INT_EQ(
        Star_Interpret(&vm, test_nop, sizeof(test_nop) - 1),
        STAR_VM_RESULT_FAULT);
    return SUCCESS_INDICATOR;
}

static int test_nop2(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    
    STAR_VM_INIT(&vm);
    
    /* This does have a STOP, expect OK */
    YYY_EXPECT_INT_EQ(
        Star_Interpret(&vm, test_nop, sizeof(test_nop)),
        STAR_VM_RESULT_OK);
    return SUCCESS_INDICATOR;
}

static int test_stop(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    
    STAR_VM_INIT(&vm);
    
    YYY_EXPECT_INT_EQ(
        Star_Interpret(&vm, test_nop + sizeof(test_nop) - 1, 1),
        STAR_VM_RESULT_OK);
    return SUCCESS_INDICATOR;
}

#define STAR_TEST_SIMPLE_DATA(OPCODE) \
static const unsigned char test_ ## OPCODE ## 1data[] = { \
    (OPCODE), \
    STAR_VM_NOP, \
    STAR_VM_TRAP \
}; \
static const unsigned char test_ ## OPCODE ## 2data[] = { \
    (OPCODE), \
    STAR_VM_TRAP \
}; \
static const unsigned char test_ ## OPCODE ## 3data[] = { \
    (OPCODE), \
    STAR_VM_STOP \
};

#define STAR_TEST_SIMPLE(OPCODE, SETUP, VALIDATE) do{ \
    struct Star_VM STAR_TEST_SIMPLE_test_vm; \
    int SAVE_SUCCESS_INDICATOR = SUCCESS_INDICATOR; \
    STAR_VM_INIT(&STAR_TEST_SIMPLE_test_vm); \
    SETUP(&STAR_TEST_SIMPLE_test_vm); \
    SUCCESS_INDICATOR = 1; \
    YYY_EXPECT_INT_EQ( \
        Star_Interpret( \
            &STAR_TEST_SIMPLE_test_vm, \
            test_ ## OPCODE ## 1data, \
            sizeof(test_ ## OPCODE ## 1data)), \
        STAR_VM_RESULT_TRAP); \
    if(SUCCESS_INDICATOR) { VALIDATE(&STAR_TEST_SIMPLE_test_vm); } \
    if(!SUCCESS_INDICATOR) SAVE_SUCCESS_INDICATOR = 0; \
    SUCCESS_INDICATOR = 1; \
    STAR_VM_INIT(&STAR_TEST_SIMPLE_test_vm); \
    SETUP(&STAR_TEST_SIMPLE_test_vm); \
    YYY_EXPECT_INT_EQ( \
        Star_Interpret( \
            &STAR_TEST_SIMPLE_test_vm, \
            test_ ## OPCODE ## 2data, \
            sizeof(test_ ## OPCODE ## 2data)), \
        STAR_VM_RESULT_TRAP); \
    if(SUCCESS_INDICATOR) { VALIDATE(&STAR_TEST_SIMPLE_test_vm); } \
    if(!SUCCESS_INDICATOR) SAVE_SUCCESS_INDICATOR = 0; \
    SUCCESS_INDICATOR = 1; \
    STAR_VM_INIT(&STAR_TEST_SIMPLE_test_vm); \
    SETUP(&STAR_TEST_SIMPLE_test_vm); \
    YYY_EXPECT_INT_EQ( \
        Star_Interpret( \
            &STAR_TEST_SIMPLE_test_vm, \
            test_ ## OPCODE ## 3data, \
            sizeof(test_ ## OPCODE ## 3data)), \
        STAR_VM_RESULT_OK); \
    if(SUCCESS_INDICATOR) { VALIDATE(&STAR_TEST_SIMPLE_test_vm); } \
    if(!SAVE_SUCCESS_INDICATOR) SUCCESS_INDICATOR = 0; \
}while(0)

/*****************************************************************************/
/* PUSHA */
STAR_TEST_SIMPLE_DATA(STAR_VM_PUSHA)

static int test_pusha(){
    int SUCCESS_INDICATOR = 1;
    int val;
#define STAR_VM_PUSHA_SETUP(VM) do{ \
    (VM)->a = 0x0216;  \
    (VM)->b = 9999; \
}while(0)

#define STAR_VM_PUSHA_VALIDATE(VM) do{ \
    YYY_EXPECT_TRUE(STAR_VM_CAN_POP(VM));  \
    val = STAR_VM_POP(VM); \
    YYY_EXPECT_INT_EQ(val, 0x0216); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_PUSHA,
        STAR_VM_PUSHA_SETUP,
        STAR_VM_PUSHA_VALIDATE);
    return SUCCESS_INDICATOR;

#undef STAR_VM_PUSHA_SETUP
#undef STAR_VM_PUSHA_VALIDATE
}

/*****************************************************************************/
/* POPA */
STAR_TEST_SIMPLE_DATA(STAR_VM_POPA)

static int test_popa(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_POPA_SETUP(VM) do{ \
    STAR_VM_PUSH(VM, 0xCDAB); \
    (VM)->b = 9999; \
}while(0)

#define STAR_VM_POPA_VALIDATE(VM) do{ \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
    YYY_EXPECT_INT_EQ((VM)->a, 0xCDAB); \
    YYY_EXPECT_INT_EQ((VM)->b, 9999); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_POPA,
        STAR_VM_POPA_SETUP,
        STAR_VM_POPA_VALIDATE);
    return SUCCESS_INDICATOR;
#undef STAR_VM_POPA_SETUP
#undef STAR_VM_POPA_VALIDATE
}

/*****************************************************************************/
/* PUSHB */
STAR_TEST_SIMPLE_DATA(STAR_VM_PUSHB)

static int test_pushb(){
    int SUCCESS_INDICATOR = 1;
    int val;
#define STAR_VM_PUSHB_SETUP(VM) do{ \
    (VM)->b = 0x0216;  \
    (VM)->a = 9999; \
}while(0)

#define STAR_VM_PUSHB_VALIDATE(VM) do{ \
    YYY_EXPECT_TRUE(STAR_VM_CAN_POP(VM));  \
    val = STAR_VM_POP(VM); \
    YYY_EXPECT_INT_EQ(val, 0x0216); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_PUSHB,
        STAR_VM_PUSHB_SETUP,
        STAR_VM_PUSHB_VALIDATE);
    return SUCCESS_INDICATOR;

#undef STAR_VM_PUSHB_SETUP
#undef STAR_VM_PUSHB_VALIDATE
}

/*****************************************************************************/
/* POPB */
STAR_TEST_SIMPLE_DATA(STAR_VM_POPB)

static int test_popb(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_POPB_SETUP(VM) do{ \
    STAR_VM_PUSH(VM, 0xCDAB); \
    (VM)->a = 9999; \
}while(0)

#define STAR_VM_POPB_VALIDATE(VM) do{ \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
    YYY_EXPECT_INT_EQ((VM)->a, 9999); \
    YYY_EXPECT_INT_EQ((VM)->b, 0xCDAB); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_POPB,
        STAR_VM_POPB_SETUP,
        STAR_VM_POPB_VALIDATE);
    return SUCCESS_INDICATOR;

#undef STAR_VM_POPB_SETUP
#undef STAR_VM_POPB_VALIDATE
}

/*****************************************************************************/
/* SWAP */
STAR_TEST_SIMPLE_DATA(STAR_VM_SWAP)

static int test_swap(){
    int SUCCESS_INDICATOR = 1;
    int val;
#define STAR_VM_SWAP_SETUP(VM) do{ \
    STAR_VM_PUSH(VM, 0xAABB); \
    STAR_VM_PUSH(VM, 0xCCDD); \
    (VM)->a = 0xEEFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_SWAP_VALIDATE(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xEEFF); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE(STAR_VM_CAN_POP(VM)); \
    val = STAR_VM_POP(VM); \
    YYY_EXPECT_INT_EQ(val, 0xAABB); \
    YYY_EXPECT_TRUE(STAR_VM_CAN_POP(VM)); \
    val = STAR_VM_POP(VM); \
    YYY_EXPECT_INT_EQ(val, 0xCCDD); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_SWAP,
        STAR_VM_SWAP_SETUP,
        STAR_VM_SWAP_VALIDATE);
    return SUCCESS_INDICATOR;

#undef STAR_VM_SWAP_SETUP
#undef STAR_VM_SWAP_VALIDATE
}

/*****************************************************************************/
/* DUP */
STAR_TEST_SIMPLE_DATA(STAR_VM_DUP)

static int test_dup(){
    int SUCCESS_INDICATOR = 1;
    int val;
#define STAR_VM_DUP_SETUP(VM) do{ \
    STAR_VM_PUSH(VM, 0xAABB); \
    STAR_VM_PUSH(VM, 0xCCDD); \
    (VM)->a = 0xEEFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_DUP_VALIDATE(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xEEFF); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE(STAR_VM_CAN_POP(VM)); \
    val = STAR_VM_POP(VM); \
    YYY_EXPECT_INT_EQ(val, 0xCCDD); \
    YYY_EXPECT_TRUE(STAR_VM_CAN_POP(VM)); \
    val = STAR_VM_POP(VM); \
    YYY_EXPECT_INT_EQ(val, 0xCCDD); \
    YYY_EXPECT_TRUE(STAR_VM_CAN_POP(VM)); \
    val = STAR_VM_POP(VM); \
    YYY_EXPECT_INT_EQ(val, 0xAABB); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_DUP,
        STAR_VM_DUP_SETUP,
        STAR_VM_DUP_VALIDATE);
    return SUCCESS_INDICATOR;

#undef STAR_VM_DUP_SETUP
#undef STAR_VM_DUP_VALIDATE
}

/*****************************************************************************/
/* ZERO */
STAR_TEST_SIMPLE_DATA(STAR_VM_ZERO)

static int test_zero(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_ZERO_SETUP(VM) do{ \
    (VM)->a = 0xEEFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_ZERO_VALIDATE(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_ZERO,
        STAR_VM_ZERO_SETUP,
        STAR_VM_ZERO_VALIDATE);
    return SUCCESS_INDICATOR;

#undef STAR_VM_ZERO_SETUP
#undef STAR_VM_ZERO_VALIDATE
}

/*****************************************************************************/
/* ONE */
STAR_TEST_SIMPLE_DATA(STAR_VM_ONE)

static int test_one(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_ONE_SETUP(VM) do{ \
    (VM)->a = 0xEEFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_ONE_VALIDATE(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 1); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_ONE,
        STAR_VM_ONE_SETUP,
        STAR_VM_ONE_VALIDATE);
    return SUCCESS_INDICATOR;

#undef STAR_VM_ONE_SETUP
#undef STAR_VM_ONE_VALIDATE
}

/*****************************************************************************/
/* INC */
STAR_TEST_SIMPLE_DATA(STAR_VM_INC)

static int test_inc(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_INC_SETUP_0(VM) do{ \
    (VM)->a = 0; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_INC_VALIDATE_0(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 1); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_INC_SETUP_1(VM) do{ \
    (VM)->a = 0x00FF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_INC_VALIDATE_1(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x0100); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_INC_SETUP_2(VM) do{ \
    (VM)->a = 0xFFFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_INC_VALIDATE_2(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_INC_SETUP_3(VM) do{ \
    (VM)->a = 0x2397; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_INC_VALIDATE_3(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x2398); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_INC,
        STAR_VM_INC_SETUP_0,
        STAR_VM_INC_VALIDATE_0);
    STAR_TEST_SIMPLE(
        STAR_VM_INC,
        STAR_VM_INC_SETUP_1,
        STAR_VM_INC_VALIDATE_1);
    STAR_TEST_SIMPLE(
        STAR_VM_INC,
        STAR_VM_INC_SETUP_2,
        STAR_VM_INC_VALIDATE_2);
    STAR_TEST_SIMPLE(
        STAR_VM_INC,
        STAR_VM_INC_SETUP_3,
        STAR_VM_INC_VALIDATE_3);
    return SUCCESS_INDICATOR;

#undef STAR_VM_INC_SETUP_0
#undef STAR_VM_INC_VALIDATE_0
#undef STAR_VM_INC_SETUP_1
#undef STAR_VM_INC_VALIDATE_1
#undef STAR_VM_INC_SETUP_2
#undef STAR_VM_INC_VALIDATE_2
#undef STAR_VM_INC_SETUP_3
#undef STAR_VM_INC_VALIDATE_3
}

/*****************************************************************************/
/* DEC */
STAR_TEST_SIMPLE_DATA(STAR_VM_DEC)

static int test_dec(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_DEC_SETUP_0(VM) do{ \
    (VM)->a = 0; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_DEC_VALIDATE_0(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xFFFF); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_DEC_SETUP_1(VM) do{ \
    (VM)->a = 0x0100; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_DEC_VALIDATE_1(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x00FF); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_DEC_SETUP_2(VM) do{ \
    (VM)->a = 0xFFFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_DEC_VALIDATE_2(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xFFFE); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_DEC_SETUP_3(VM) do{ \
    (VM)->a = 1; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_DEC_VALIDATE_3(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_DEC_SETUP_4(VM) do{ \
    (VM)->a = 0x2397; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_DEC_VALIDATE_4(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x2396); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_DEC,
        STAR_VM_DEC_SETUP_0,
        STAR_VM_DEC_VALIDATE_0);
    STAR_TEST_SIMPLE(
        STAR_VM_DEC,
        STAR_VM_DEC_SETUP_1,
        STAR_VM_DEC_VALIDATE_1);
    STAR_TEST_SIMPLE(
        STAR_VM_DEC,
        STAR_VM_DEC_SETUP_2,
        STAR_VM_DEC_VALIDATE_2);
    STAR_TEST_SIMPLE(
        STAR_VM_DEC,
        STAR_VM_DEC_SETUP_3,
        STAR_VM_DEC_VALIDATE_3);
    STAR_TEST_SIMPLE(
        STAR_VM_DEC,
        STAR_VM_DEC_SETUP_4,
        STAR_VM_DEC_VALIDATE_4);
    return SUCCESS_INDICATOR;

#undef STAR_VM_DEC_SETUP_0
#undef STAR_VM_DEC_VALIDATE_0
#undef STAR_VM_DEC_SETUP_1
#undef STAR_VM_DEC_VALIDATE_1
#undef STAR_VM_DEC_SETUP_2
#undef STAR_VM_DEC_VALIDATE_2
#undef STAR_VM_DEC_SETUP_3
#undef STAR_VM_DEC_VALIDATE_3
#undef STAR_VM_DEC_SETUP_4
#undef STAR_VM_DEC_VALIDATE_4
}

/*****************************************************************************/
/* ADD */
STAR_TEST_SIMPLE_DATA(STAR_VM_ADD)

static int test_add(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_ADD_SETUP_0(VM) do{ \
    (VM)->a = 0; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0); \
}while(0)

#define STAR_VM_ADD_VALIDATE_0(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_ADD_SETUP_1(VM) do{ \
    (VM)->a = 0; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0x0099); \
}while(0)

#define STAR_VM_ADD_VALIDATE_1(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x0099); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_ADD_SETUP_2(VM) do{ \
    (VM)->a = 0x00AF; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0x0097); \
}while(0)

#define STAR_VM_ADD_VALIDATE_2(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x0146); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_ADD_SETUP_3(VM) do{ \
    (VM)->a = 0xEFAD; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0x8921); \
}while(0)

#define STAR_VM_ADD_VALIDATE_3(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x78CE); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_ADD_SETUP_4(VM) do{ \
    (VM)->a = 0xFF81; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0x00AD); \
}while(0)

#define STAR_VM_ADD_VALIDATE_4(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x002E); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_ADD,
        STAR_VM_ADD_SETUP_0,
        STAR_VM_ADD_VALIDATE_0);
    STAR_TEST_SIMPLE(
        STAR_VM_ADD,
        STAR_VM_ADD_SETUP_1,
        STAR_VM_ADD_VALIDATE_1);
    STAR_TEST_SIMPLE(
        STAR_VM_ADD,
        STAR_VM_ADD_SETUP_2,
        STAR_VM_ADD_VALIDATE_2);
    STAR_TEST_SIMPLE(
        STAR_VM_ADD,
        STAR_VM_ADD_SETUP_3,
        STAR_VM_ADD_VALIDATE_3);
    STAR_TEST_SIMPLE(
        STAR_VM_ADD,
        STAR_VM_ADD_SETUP_4,
        STAR_VM_ADD_VALIDATE_4);
    return SUCCESS_INDICATOR;

#undef STAR_VM_ADD_SETUP_0
#undef STAR_VM_ADD_VALIDATE_0
#undef STAR_VM_ADD_SETUP_1
#undef STAR_VM_ADD_VALIDATE_1
#undef STAR_VM_ADD_SETUP_2
#undef STAR_VM_ADD_VALIDATE_2
#undef STAR_VM_ADD_SETUP_3
#undef STAR_VM_ADD_VALIDATE_3
#undef STAR_VM_ADD_SETUP_4
#undef STAR_VM_ADD_VALIDATE_4
}

/*****************************************************************************/
/* SUB */
STAR_TEST_SIMPLE_DATA(STAR_VM_SUB)

static int test_sub(){
    int SUCCESS_INDICATOR = 1;
#define STAR_VM_SUB_SETUP_0(VM) do{ \
    (VM)->a = 0; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0x0010); \
}while(0)

#define STAR_VM_SUB_VALIDATE_0(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xFFF0); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_SUB_SETUP_1(VM) do{ \
    (VM)->a = 0x9876; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0x1111); \
}while(0)

#define STAR_VM_SUB_VALIDATE_1(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0x8765); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_SUB_SETUP_2(VM) do{ \
    (VM)->a = 0xABCD; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0xABCD); \
}while(0)

#define STAR_VM_SUB_VALIDATE_2(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)

#define STAR_VM_SUB_SETUP_3(VM) do{ \
    (VM)->a = 0xABCD; \
    (VM)->b = 0x5566; \
    STAR_VM_PUSH((VM), 0xFEDA); \
}while(0)

#define STAR_VM_SUB_VALIDATE_3(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xACF3); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE((VM)->t); \
    YYY_EXPECT_FALSE(STAR_VM_CAN_POP(VM)); \
}while(0)
    
    STAR_TEST_SIMPLE(
        STAR_VM_SUB,
        STAR_VM_SUB_SETUP_0,
        STAR_VM_SUB_VALIDATE_0);
    STAR_TEST_SIMPLE(
        STAR_VM_SUB,
        STAR_VM_SUB_SETUP_1,
        STAR_VM_SUB_VALIDATE_1);
    STAR_TEST_SIMPLE(
        STAR_VM_SUB,
        STAR_VM_SUB_SETUP_2,
        STAR_VM_SUB_VALIDATE_2);
    STAR_TEST_SIMPLE(
        STAR_VM_SUB,
        STAR_VM_SUB_SETUP_3,
        STAR_VM_SUB_VALIDATE_3);
    return SUCCESS_INDICATOR;

#undef STAR_VM_SUB_SETUP_0
#undef STAR_VM_SUB_VALIDATE_0
#undef STAR_VM_SUB_SETUP_1
#undef STAR_VM_SUB_VALIDATE_1
#undef STAR_VM_SUB_SETUP_2
#undef STAR_VM_SUB_VALIDATE_2
#undef STAR_VM_SUB_SETUP_3
#undef STAR_VM_SUB_VALIDATE_3
}

/*****************************************************************************/
/* TODO! */
static int test_mul(){ return 1; }
static int test_div(){ return 1; }

/*****************************************************************************/

#define STAR_VM_CARRY_SETUP_0(VM) do{ \
    (VM)->t = 0; \
    (VM)->a = 0xEEFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_CARRY_SETUP_1(VM) do{ \
    (VM)->t = 1; \
    (VM)->a = 0xEEFF; \
    (VM)->b = 0x5566; \
}while(0)

#define STAR_VM_CARRY_VALIDATE_0(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xEEFF); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_FALSE((VM)->t); \
}while(0)

#define STAR_VM_CARRY_VALIDATE_1(VM) do{ \
    YYY_EXPECT_INT_EQ((VM)->a, 0xEEFF); \
    YYY_EXPECT_INT_EQ((VM)->b, 0x5566); \
    YYY_EXPECT_TRUE((VM)->t); \
}while(0)

/*****************************************************************************/
/* CT */
STAR_TEST_SIMPLE_DATA(STAR_VM_CT)

static int test_ct(){
    int SUCCESS_INDICATOR = 1;
    STAR_TEST_SIMPLE(
        STAR_VM_CT,
        STAR_VM_CARRY_SETUP_0,
        STAR_VM_CARRY_VALIDATE_0);
    STAR_TEST_SIMPLE(
        STAR_VM_CT,
        STAR_VM_CARRY_SETUP_1,
        STAR_VM_CARRY_VALIDATE_0);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/
/* ST */
STAR_TEST_SIMPLE_DATA(STAR_VM_ST)

static int test_st(){
    int SUCCESS_INDICATOR = 1;
    STAR_TEST_SIMPLE(
        STAR_VM_ST,
        STAR_VM_CARRY_SETUP_0,
        STAR_VM_CARRY_VALIDATE_1);
    STAR_TEST_SIMPLE(
        STAR_VM_ST,
        STAR_VM_CARRY_SETUP_1,
        STAR_VM_CARRY_VALIDATE_1);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/
/* CLT */
STAR_TEST_SIMPLE_DATA(STAR_VM_CLT)

static int test_clt(){
    int SUCCESS_INDICATOR = 1;
    STAR_TEST_SIMPLE(
        STAR_VM_CLT,
        STAR_VM_CARRY_SETUP_0,
        STAR_VM_CARRY_VALIDATE_1);
    STAR_TEST_SIMPLE(
        STAR_VM_CLT,
        STAR_VM_CARRY_SETUP_1,
        STAR_VM_CARRY_VALIDATE_0);
    return SUCCESS_INDICATOR;
}

#undef STAR_VM_CARRY_SETUP_0
#undef STAR_VM_CARRY_SETUP_1
#undef STAR_VM_CARRY_VALIDATE_0
#undef STAR_VM_CARRY_VALIDATE_1

/*****************************************************************************/
/* Some intermediate arithmetic integration tests. */
static const unsigned char test_add_data[] = {
    STAR_VM_SET, 0, 131,
    STAR_VM_PUSHA,
    STAR_VM_SET, 0, 97,
    STAR_VM_ADD,
    STAR_VM_TRAP
};

static int test_add1(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    STAR_VM_INIT(&vm);
    
    /* Test without the set, manually setting the values. */
    vm.a = 97;
    STAR_VM_PUSH(&vm, 131);
    YYY_ASSERT_INT_EQ(
        Star_Interpret(&vm, test_add_data + 7, sizeof(test_add_data) - 7),
        STAR_VM_RESULT_TRAP);
    YYY_EXPECT_INT_EQ(vm.a, 228);
    YYY_EXPECT_FALSE(vm.t);
    return SUCCESS_INDICATOR;
}

static int test_add2(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    STAR_VM_INIT(&vm);
    
    /* Test without the push, manually pushing the values. */
    STAR_VM_PUSH(&vm, 131);
    YYY_ASSERT_INT_EQ(
        Star_Interpret(&vm, test_add_data + 4, sizeof(test_add_data) - 4),
        STAR_VM_RESULT_TRAP);
    YYY_EXPECT_INT_EQ(vm.a, 228);
    YYY_EXPECT_FALSE(vm.t);
    return SUCCESS_INDICATOR;
}

static int test_add3(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    STAR_VM_INIT(&vm);
    
    /* Test without the push, manually pushing the values. */
    YYY_ASSERT_INT_EQ(
        Star_Interpret(&vm, test_add_data, sizeof(test_add_data)),
        STAR_VM_RESULT_TRAP);
    YYY_EXPECT_INT_EQ(vm.a, 228);
    YYY_EXPECT_FALSE(vm.t);
    return SUCCESS_INDICATOR;
}

/*****************************************************************************/
/* Jump tests. */
static const unsigned char test_jp1_data[] = {
    STAR_VM_JP, 0, 4,
    STAR_VM_STOP,
    STAR_VM_TRAP
};

static int test_jp1(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    STAR_VM_INIT(&vm);
    
    YYY_ASSERT_INT_EQ(
        Star_Interpret(&vm, test_jp1_data, sizeof(test_jp1_data)),
        STAR_VM_RESULT_TRAP);
    return SUCCESS_INDICATOR;
}

static const unsigned char test_jp2_data[] = {
    STAR_VM_SET, 0xAB, 0xCD,
    STAR_VM_JP, 0, 9,
    STAR_VM_SET, 0x12, 0x34,
    STAR_VM_STOP,
};

static int test_jp2(){
    int SUCCESS_INDICATOR = 1;
    struct Star_VM vm;
    STAR_VM_INIT(&vm);
    
    YYY_ASSERT_INT_EQ(
        Star_Interpret(&vm, test_jp2_data, sizeof(test_jp2_data)),
        STAR_VM_RESULT_OK);
    YYY_ASSERT_INT_EQ(vm.a, 0xABCD);
    return SUCCESS_INDICATOR;
}
/*****************************************************************************/

static struct YYY_Test vm_tests[] = {
    YYY_TEST(test_nop1),
    YYY_TEST(test_nop2),
    YYY_TEST(test_pusha),
    YYY_TEST(test_popa),
    YYY_TEST(test_pushb),
    YYY_TEST(test_popb),
    YYY_TEST(test_swap),
    YYY_TEST(test_dup),
    YYY_TEST(test_zero),
    YYY_TEST(test_one),
    YYY_TEST(test_inc),
    YYY_TEST(test_add),
    YYY_TEST(test_sub),
    YYY_TEST(test_mul),
    YYY_TEST(test_div),
    YYY_TEST(test_dec),
    YYY_TEST(test_ct),
    YYY_TEST(test_st),
    YYY_TEST(test_clt),
    YYY_TEST(test_stop),
    YYY_TEST(test_add1),
    YYY_TEST(test_add2),
    YYY_TEST(test_add3),
    YYY_TEST(test_jp1),
    YYY_TEST(test_jp2)
};

YYY_TEST_FUNCTION(VM_Test, vm_tests, "vm_tests")

int main(){
    int ok = 0;
    YYY_RUN_TEST_SUITE(VM_Test, ok, "Star VM Tests");
    return ok;
}

