/* Copyright (c) 2023 AlaskanEmily
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *  
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 * 
 *   3. Trans rights are human rights.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#ifndef STAR_VM_H
#define STAR_VM_H

/*****************************************************************************/

#define STAR_VM_STACK_SIZE 123
#define STAR_VM_RESULT_OK 0
#define STAR_VM_RESULT_TRAP 1
#define STAR_VM_RESULT_FAULT -1
#define STAR_VM_RESULT_STACK_UNDERFLOW -2
#define STAR_VM_RESULT_STACK_OVERFLOW -3

#define STAR_VM_NOP     0
#define STAR_VM_PUSHA   1
#define STAR_VM_POPA    2
#define STAR_VM_PUSHB   3
#define STAR_VM_POPB    4
#define STAR_VM_SWAP    5
#define STAR_VM_DUP     6
#define STAR_VM_ZERO    7
#define STAR_VM_ONE     8
#define STAR_VM_SET     9
#define STAR_VM_ADD     10
#define STAR_VM_SUB     11
#define STAR_VM_MUL     12
#define STAR_VM_DIV     13
#define STAR_VM_REM     14
#define STAR_VM_INC     15
#define STAR_VM_DEC     16
#define STAR_VM_AND     17
#define STAR_VM_OR      18
#define STAR_VM_XOR     19
#define STAR_VM_NOT     20
#define STAR_VM_SHL     21
#define STAR_VM_SHR     22
#define STAR_VM_CMP     23
#define STAR_VM_TEST    24
#define STAR_VM_BIT     25
#define STAR_VM_CT      26
#define STAR_VM_ST      27
#define STAR_VM_CLT     28
#define STAR_VM_JP      29
#define STAR_VM_JPT     30
#define STAR_VM_JPF     31
#define STAR_VM_JR      32
#define STAR_VM_JRT     33
#define STAR_VM_JRF     34
#define STAR_VM_CALL    35
#define STAR_VM_RET     36
#define STAR_VM_TRAP    37
#define STAR_VM_STOP    38

/*****************************************************************************/

#ifdef __cplusplus
#pragma once
extern "C"{
#endif

/*****************************************************************************/

#ifndef __ASSEMBLY__

/*****************************************************************************/

struct Star_VM{
    unsigned short a, b, pc, sp;
    unsigned char t; /* Only bit 0 is guaranteed to be set/cleared. */
    unsigned char pad;
    unsigned short stack[STAR_VM_STACK_SIZE];
};

/*****************************************************************************/

#define STAR_VM_INIT(VM) do{ \
    (VM)->a = (VM)->b = (VM)->pc = 0; \
    (VM)->t = 0; \
    (VM)->sp = STAR_VM_STACK_SIZE; \
}while(0)

/* Helpers, intended to allow callers to setup the VM in specific states. */
#define STAR_VM_CAN_POP(VM) ((VM)->sp < STAR_VM_STACK_SIZE) \

#define STAR_VM_CAN_PUSH(VM) ((VM)->sp >= 0)

#define STAR_VM_PUSH(VM, VAL) ((VM)->stack[--((VM)->sp)] = (VAL))

#define STAR_VM_POP(VM) ((VM)->stack[(VM)->sp++])

/*****************************************************************************/

int Star_Interpret(struct Star_VM *vm, const void *prog, unsigned len);

/*****************************************************************************/

#endif

/*****************************************************************************/

#ifdef __cplusplus
} // extern "C"
#endif

/*****************************************************************************/

#endif /* STAR_VM_H */

