# Any copyright is dedicated to the Public Domain.
# https://creativecommons.org/publicdomain/zero/1.0/

# clang/GCC BSD makefile.

.ifndef STAR_CC
STAR_CC!=which egcc 2>/dev/null || which clang 2>/dev/null || which gcc 2>/dev/null || echo cc
.endif

STAR_CFLAGS?=-O1 -Wall -Wextra -pedantic -fpic -g -ansi
STAR_LD?=$(STAR_CC)
STAR_LDFLAGS?=-g -fpic
STAR_AR?=ar
STAR_ARFLAGS?=-rc
STAR_RANLIB?=ranlib
# We can depend(ish) on these existing in BSD.
STAR_YACC?=yacc
STAR_FLEXFLAGS?=-8 -l
STAR_FLEX?=flex
STAR_YACCFLAGS?=-d -b y

.if "${MACHINE}" == "i386" || "${MACHINE}" == "i686"
STAR_TARGET?=x86
.else
STAR_TARGET?=none
.endif

.if "${STAR_TARGET}" == "x86"
.ifndef STAR_YASM
STAR_YASM!=which yasm 2>/dev/null || which nasm 2>/dev/null || echo nasm
.endif
STAR_YASMFLAGS?=-f elf32 -a x86 -g stabs 
.endif

# TODO: Used with 68K only?
STAR_ASFLAGS=--traditional-format -gstabs -mnaked-reg

RM?=rm -f
MKDIR?=mkdir -p
INSTALL?=install

.include "star.mk"

.PHONY: clean check distclean
.IGNORE: clean distclean

