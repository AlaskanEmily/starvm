/* Copyright (c) 2023 AlaskanEmily
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *  
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 * 
 *   3. Trans rights are human rights.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#ifndef STAR_ASM_H
#define STAR_ASM_H
#pragma once

#include <stdio.h>

/*****************************************************************************/

#ifdef __cplusplus
extern "C"{
#endif

/*****************************************************************************/

extern int star_line;
extern int star_error;

/*****************************************************************************/

#if (defined __GNUC__) && !(defined __TINYC__)
__attribute__((pure))
#endif
int Star_ASM_CheckOpcode(const char *text);

/*****************************************************************************/

void Star_ASM_Init(void);

/*****************************************************************************/

void Star_ASM_Emit(int o, ...);

/*****************************************************************************/

void Star_ASM_Label(char *l);

/*****************************************************************************/

int Star_ASM_Write(FILE *f);

/*****************************************************************************/

void Star_ASM_Error(const char *s);

/*****************************************************************************/

#ifdef __cplusplus
} // extern "C"
#endif

/*****************************************************************************/

#endif /* STAR_ASM_H */

