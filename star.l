%{
/* Copyright (c) 2023 AlaskanEmily
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *  
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 * 
 *   3. Trans rights are human rights.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

#define YY_SKIP_YYWRAP
#define YY_NO_UNPUT
#define YY_NO_INPUT
#define yywrap() 1
#include <string.h>
#include "y.tab.h"
#include "star_asm.h"

#undef input
#undef unput

extern YYSTYPE yylval;

%}

A [$.a-zA-Z_]
B [$.a-zA-Z0-9_]
D [0-9]
O [0-7]
H [0-9a-fA-F]
WS [ \t\r\v]

%%

{WS}+       { (void)0; }
";".*       { (void)0; }

{A}{B}*     {
    yylval.i = Star_ASM_CheckOpcode(yytext);
    if(yylval.i >= 0)
        return OPCODE;
    yylval.l = yytext;
    return LABEL;
}
"0x"{H}+    { yylval.i = strtol(yytext + 2, NULL, 16); return INTEGER; }
"0"{O}*     { yylval.i = strtol(yytext, NULL, 8); return INTEGER; }
{D}+        { yylval.i = atoi(yytext); return INTEGER; }
":"         { return ':'; }
"\n"        { return '\n'; }

