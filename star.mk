# Any copyright is dedicated to the Public Domain.
# https://creativecommons.org/publicdomain/zero/1.0/

all: libstarvm.a staras1

# Rule to copy from the target to the unified name.
libstarvm.a: $(STAR_TARGET)/libstarvm.a
	$(INSTALL) $(STAR_TARGET)/libstarvm.a $@

# Non-ASM implementation.
none/libstarvm.a: star_vm.o
	$(MKDIR) none
	$(STAR_AR) $(STAR_ARFLAGS) $@ star_vm.o
	$(STAR_RANLIB) $@

star_vm.o: star_vm.c star_vm.h
	$(STAR_CC) $(STAR_CFLAGS) -c -o $@ star_vm.c

# x86 implementation
x86/libstarvm.a: star_x86.o
	$(STAR_AR) $(STAR_ARFLAGS) $@ star_x86.o
	$(STAR_RANLIB) $@

star_x86.o: star_x86.s star_vm.inc
	$(STAR_YASM) $(STAR_YASMFLAGS) -o $@ star_x86.s

# 68K implementation
68k/libstarvm.a: star_68k.o
	$(STAR_AR) $(STAR_ARFLAGS) $@ star_68k.o
	$(STAR_RANLIB) $@

star_68k.o: star_68k.S star_vm.h
	$(STAR_AS) $(STAR_ASFLAGS) -o $@ star_68k.S

# Assembler.
lex.yy.c: star.l
	$(STAR_FLEX) $(STAR_FLEXFLAGS) star.l

y.tab.c y.tab.h: star.y
	$(STAR_YACC) $(STAR_YACCFLAGS) star.y

star_asm.o: star_asm.c star_asm.h y.tab.h
	$(STAR_CC) $(STAR_CFLAGS) -c -o $@ star_asm.c

lex.yy.o: lex.yy.c
	$(STAR_CC) $(STAR_CFLAGS) -c -o $@ lex.yy.c

y.tab.o: y.tab.c star_asm.h
	$(STAR_CC) $(STAR_CFLAGS) -c -o $@ y.tab.c

AS1OBJECTS=star_asm.o lex.yy.o y.tab.o
staras1: $(AS1OBJECTS)
	$(STAR_LD) $(STAR_LDFLAGS) -o $@ $(AS1OBJECTS)

clean:
	$(RM) *.a */*.a *.o startest staras1

distclean: clean
	$(RM) y.tab.c y.tab.h lex.yy.c

startest: test.c star_vm.h yyy_test.h libstarvm.a
	$(STAR_CC) $(STAR_CFLAGS) -o $@ test.c libstarvm.a

check: startest
	exec ./startest

.PHONY: clean check distclean
.IGNORE: clean distclean

