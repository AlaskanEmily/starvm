; Copyright (c) 2023 AlaskanEmily
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions
; are met:
; 
;   1. Redistributions of source code must retain the above copyright
;      notice, this list of conditions and the following disclaimer.
;  
;   2. Redistributions in binary form must reproduce the above copyright
;      notice, this list of conditions and the following disclaimer in
;      the documentation and/or other materials provided with the
;      distribution.
; 
;   3. Trans rights are human rights.
;  
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
; INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
; BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
; OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
; AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
; THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
; DAMAGE.
;
; YASM/NASM implementation of the Star VM.
; Constants for MOT/YASM/TASM/etc implementations of the Star VM.

STAR_VM_STACK_SIZE equ 123
STAR_VM_RESULT_OK equ 0
STAR_VM_RESULT_TRAP equ 1
STAR_VM_RESULT_FAULT equ -1
STAR_VM_RESULT_STACK_UNDERFLOW equ -2
STAR_VM_RESULT_STACK_OVERFLOW equ -3

STAR_VM_NOP    equ 0
STAR_VM_PUSHA  equ 1
STAR_VM_POPA   equ 2
STAR_VM_PUSHB  equ 3
STAR_VM_POPB   equ 4
STAR_VM_SWAP   equ 5
STAR_VM_DUP    equ 6
STAR_VM_ZERO   equ 7
STAR_VM_ONE    equ 8
STAR_VM_SET    equ 9
STAR_VM_ADD    equ 10
STAR_VM_SUB    equ 11
STAR_VM_MUL    equ 12
STAR_VM_DIV    equ 13
STAR_VM_REM    equ 14
STAR_VM_INC    equ 15
STAR_VM_DEC    equ 16
STAR_VM_AND    equ 17
STAR_VM_OR     equ 18
STAR_VM_XOR    equ 19
STAR_VM_NOT    equ 20
STAR_VM_SHL    equ 21
STAR_VM_SHR    equ 22
STAR_VM_CMP    equ 23
STAR_VM_TEST   equ 24
STAR_VM_BIT    equ 25
STAR_VM_CT     equ 26
STAR_VM_ST     equ 27
STAR_VM_CLT    equ 28
STAR_VM_JP     equ 29
STAR_VM_JPT    equ 30
STAR_VM_JPF    equ 31
STAR_VM_JR     equ 32
STAR_VM_JRT    equ 33
STAR_VM_JRF    equ 34
STAR_VM_CALL   equ 35
STAR_VM_RET    equ 36
STAR_VM_TRAP   equ 37
STAR_VM_STOP   equ 38

STAR_VM_A_REG equ 0
STAR_VM_B_REG equ 2
STAR_VM_PC_REG equ 4
STAR_VM_SP_REG equ 6
STAR_VM_T equ 8
STAR_VM_STACK equ 10

