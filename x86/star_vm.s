; Copyright (c) 2023 AlaskanEmily
; 
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions
; are met:
; 
;   1. Redistributions of source code must retain the above copyright
;      notice, this list of conditions and the following disclaimer.
;  
;   2. Redistributions in binary form must reproduce the above copyright
;      notice, this list of conditions and the following disclaimer in
;      the documentation and/or other materials provided with the
;      distribution.
; 
;   3. Trans rights are human rights.
;  
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
; INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
; BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
; OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
; AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
; THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
; DAMAGE.
;
; YASM/NASM implementation of the Star VM.

USE32

%include "star_vm.inc"

STAR_VM_INSTRUCTION_SHIFT equ 5
STAR_VM_INSTRUCTION_SIZE equ (1 << STAR_VM_INSTRUCTION_SHIFT)

; int Star_Interpret(struct Star_VM *vm, const void *prog, unsigned len)
global Star_Interpret
Star_Interpret:
    ; esp + 4 = vm
    ; esp + 8 = prog
    ; esp + 12 = len
    mov ecx, [esp + 12]
    
    push ebp
    push edi
    push esi
    push ebx
    
    mov edi, [esp + 20]
    mov ebx, [esp + 24]
    movzx edx, WORD [edi + STAR_VM_A_REG]
    movzx esi, WORD [edi + STAR_VM_PC_REG]
    movzx ebp, WORD [edi + STAR_VM_SP_REG]
    ; eax = scratch
    ; edx = a
    ; esi = pc
    ; ebp = sp
    ; ebx = prog
    ; ecx = len
    ; edi = vm
    
%macro STAR_VM_RETURN 0-1
%if %0 > 0
%if %1 >= 0
    xor eax, eax
%if %1 > 0
    mov al, %1
%endif
%else
    mov eax, %1
%endif
%endif
    mov [edi + STAR_VM_SP_REG], bp
    mov [edi + STAR_VM_PC_REG], si
    mov [edi + STAR_VM_A_REG], dx
    pop ebx
    pop esi
    pop edi
    pop ebp
    ret
%endmacro
    
%macro STAR_VM_START_INSTRUCTION 1
global %1
%1:
.start:
%endmacro
    
%macro STAR_VM_END_INSTRUCTION 0
.final:
    times (STAR_VM_INSTRUCTION_SIZE - (.final - .start)) nop
%endmacro

%macro STAR_VM_IMM8 1
    cmp esi, ecx
    jge star_vm_fault_
    movzx e%1, BYTE [ebx + esi]
    inc esi
%endmacro

%macro STAR_VM_IMM16 1
    lea e%1, [esi + 1]
    cmp e%1, ecx
    jge star_vm_fault_
    movzx e%1, WORD [ebx + esi]
    ror %1, 8
    add esi, 2
%endmacro
    
%macro STAR_VM_DISPATCH 0
    STAR_VM_IMM8 ax
    shl eax, STAR_VM_INSTRUCTION_SHIFT
    add eax, star_vm_dispatch_
    jmp eax
%endmacro
    
%macro STAR_VM_END_INSTRUCTION_SHORT 0
    jmp star_vm_next_
    STAR_VM_END_INSTRUCTION
%endmacro

%macro STAR_VM_END_INSTRUCTION_MEDIUM 0
    cmp esi, ecx
    jl star_vm_next_
    jmp star_vm_fault_
    STAR_VM_END_INSTRUCTION
%endmacro

%macro STAR_VM_END_INSTRUCTION_LONG 0
    STAR_VM_DISPATCH
    STAR_VM_END_INSTRUCTION
%endmacro
    
%macro STAR_VM_POP_CHECK 1
    cmp ebp, STAR_VM_STACK_SIZE - %1
    jg star_vm_overflow_
%endmacro
    
%macro STAR_VM_DO_POP 1
    movzx e%1, WORD [edi + STAR_VM_STACK + ebp * 2]
    inc ebp
%endmacro
    
%macro STAR_VM_POP 1-*
    STAR_VM_POP_CHECK %0
%if %0 <= 2
%rep %0
    STAR_VM_DO_POP %1
%rotate 1
%endrep
%else
%assign i 0
%rep %0
    movzx e%1, WORD [edi + STAR_VM_STACK + (i * 2) + ebp * 2]
%assign i i + 1
%rotate 1
%endrep
    add bp, %0
%endif
%endmacro

%macro STAR_VM_PUSH_CHECK 1
    cmp ebp, %1
    jl star_vm_underflow_
%endmacro

%macro STAR_VM_DO_PUSH 1
    dec ebp
    mov WORD [edi + STAR_VM_STACK + ebp * 2], %1
%endmacro
    
%macro STAR_VM_PUSH 1-*
    STAR_VM_PUSH_CHECK %0
%if %0 <= 2
%rep %0
    STAR_VM_DO_PUSH %1
%rotate 1
%endrep
%else
%assign i 1
%rep %0
    mov WORD [edi + STAR_VM_STACK - (i * 2) + ebp * 2], %1
%assign i i + 1
%rotate 1
%endrep
    sub bp, %0
%endif
%endmacro

star_vm_next_:
    STAR_VM_IMM8 ax
star_vm_next2_:
    shl eax, STAR_VM_INSTRUCTION_SHIFT
    add eax, star_vm_dispatch_
    jmp eax
star_vm_overflow_:
    mov eax, STAR_VM_RESULT_STACK_OVERFLOW
    jmp star_vm_return_
star_vm_underflow_:
    mov eax, STAR_VM_RESULT_STACK_UNDERFLOW
    jmp star_vm_return_
star_vm_fault_:
    mov eax, STAR_VM_RESULT_FAULT
star_vm_return_:
    STAR_VM_RETURN
star_vm_dispatch_:
    
    ; 0x00 - NOP
    STAR_VM_START_INSTRUCTION star_vm_nop
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x01 - PUSHA
    STAR_VM_START_INSTRUCTION star_vm_pusha
    STAR_VM_PUSH dx
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x02 - POPA
    STAR_VM_START_INSTRUCTION star_vm_popa
    STAR_VM_POP dx
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x03 - PUSHB
    STAR_VM_START_INSTRUCTION star_vm_pushb
    movzx eax, WORD [edi + STAR_VM_B_REG]
.do:
    STAR_VM_PUSH ax
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x04 - POPB
    STAR_VM_START_INSTRUCTION star_vm_popb
    STAR_VM_POP ax
    mov [edi + STAR_VM_B_REG], ax
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x05 - SWAP
    STAR_VM_START_INSTRUCTION star_vm_swap
    STAR_VM_POP_CHECK 2
    ror DWORD [edi + STAR_VM_STACK + ebp * 2], 16
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x06 - DUP
    STAR_VM_START_INSTRUCTION star_vm_dup
    STAR_VM_POP_CHECK 1
    mov ax, [edi + STAR_VM_STACK + ebp * 2]
    ; Just barely not enough space, piggyback off the code in pushb
    jmp star_vm_pushb.do
    STAR_VM_END_INSTRUCTION
    
    ; 0x07 - ZERO
    STAR_VM_START_INSTRUCTION star_vm_zero
    xor edx, edx
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x08 - ONE
    STAR_VM_START_INSTRUCTION star_vm_one
    mov edx, 1
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x09 - SET
    STAR_VM_START_INSTRUCTION star_vm_set
    ; This is hand-written since we can combine bounds checks.
    ; This is also just going to trash register renaming for dx, so don't
    ; bother with the movzx.
    lea eax, [esi + 2]
    cmp eax, ecx
    jg star_vm_fault_
%if 1
    movzx edx, WORD [ebx + esi]
    movzx eax, BYTE [ebx + eax]
    add esi, 3
    ror dx, 8
    jmp star_vm_next2_
    STAR_VM_END_INSTRUCTION
%else
    mov dx, [ebx + esi]
    movzx eax, BYTE [ebx + eax]
    ; xor al, al
    add esi, 3
    xchg dl, dh
    shl eax, STAR_VM_INSTRUCTION_SHIFT
    add eax, star_vm_dispatch_
    jmp eax
    ; STAR_VM_END_INSTRUCTION
%endif
    
    ; 0x0A - ADD
    STAR_VM_START_INSTRUCTION star_vm_add
    ; This is also handwritten because we can squeeze more in if we combine the
    ; arithmetic and the pop.
%ifdef STAR_VM_SLOW
    STAR_VM_POP ax
    add dx, ax
    setc [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    STAR_VM_POP_CHECK 1
    add dx, [edi + STAR_VM_STACK + ebp * 2]
    setc [edi + STAR_VM_T]
    inc ebp
    STAR_VM_END_INSTRUCTION_MEDIUM
%endif
    
    ; 0x0B - SUB
    STAR_VM_START_INSTRUCTION star_vm_sub
    ; This is also handwritten because we can squeeze more in if we combine the
    ; arithmetic and the pop.
%ifdef STAR_VM_SLOW
    STAR_VM_POP ax
    sub dx, ax
    setc [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    STAR_VM_POP_CHECK 1
    sub dx, [edi + STAR_VM_STACK + ebp * 2]
    setc [edi + STAR_VM_T]
    inc ebp
    STAR_VM_END_INSTRUCTION_MEDIUM
%endif
    
    ; 0x0C MUL
    STAR_VM_START_INSTRUCTION star_vm_mul
    STAR_VM_POP ax
    mul dx
.f:
    test edx, edx
    setnz [edi + STAR_VM_T]
    mov edx, eax
    STAR_VM_END_INSTRUCTION_SHORT
    
    ; 0x0D DIV
    STAR_VM_START_INSTRUCTION star_vm_div
    mov eax, edx
    STAR_VM_POP dx
    div dx
    jmp star_vm_mul.f
    STAR_VM_END_INSTRUCTION
    
    ; 0x0E REM
    STAR_VM_START_INSTRUCTION star_vm_rem
    mov eax, edx
    STAR_VM_POP dx
    div dx
    test eax, eax
    setnz [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
    
    ; 0x0F INC
    STAR_VM_START_INSTRUCTION star_vm_inc
    inc dx
    setz [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x10 DEC
    STAR_VM_START_INSTRUCTION star_vm_dec
    test edx, edx
    setz [edi + STAR_VM_T]
    dec dx
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x11 AND
    STAR_VM_START_INSTRUCTION star_vm_and
    ; This is also handwritten because we can squeeze more in if we combine the
    ; arithmetic and the pop.
%ifdef STAR_VM_SLOW
    STAR_VM_POP ax
    and edx, eax
    setnz [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    STAR_VM_POP_CHECK 1
    and dx, [edi + STAR_VM_STACK + ebp * 2]
    setnz [edi + STAR_VM_T]
    inc ebp
    STAR_VM_END_INSTRUCTION_MEDIUM
%endif
    
    ; 0x12 OR
    STAR_VM_START_INSTRUCTION star_vm_or
    ; This is also handwritten because we can squeeze more in if we combine the
    ; arithmetic and the pop.
%ifdef STAR_VM_SLOW
    STAR_VM_POP ax
    or edx, eax
    setnz [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    STAR_VM_POP_CHECK 1
    or dx, [edi + STAR_VM_STACK + ebp * 2]
    setnz [edi + STAR_VM_T]
    inc ebp
    STAR_VM_END_INSTRUCTION_MEDIUM
%endif
    
    ; 0x13 XOR
    STAR_VM_START_INSTRUCTION star_vm_xor
    ; This is also handwritten because we can squeeze more in if we combine the
    ; arithmetic and the pop.
%ifdef STAR_VM_SLOW
    STAR_VM_POP ax
    xor edx, eax
    setnz [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    STAR_VM_POP_CHECK 1
    xor dx, [edi + STAR_VM_STACK + ebp * 2]
    setnz [edi + STAR_VM_T]
    inc ebp
    STAR_VM_END_INSTRUCTION_MEDIUM
%endif
    
    ; 0x14 NOT
    STAR_VM_START_INSTRUCTION star_vm_not
    not edx
    setnz [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x15 SHL
    STAR_VM_START_INSTRUCTION star_vm_shl
    shl edx, 1
    setc [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x16 SHR
    STAR_VM_START_INSTRUCTION star_vm_shr
    shr edx, 1
    setc [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x17 - CMP
    STAR_VM_START_INSTRUCTION star_vm_cmp
    ; This is also handwritten because we can squeeze more in if we combine the
    ; arithmetic and the pop.
%ifdef STAR_VM_SLOW
    STAR_VM_POP ax
    cmp dx, ax
    setc [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    STAR_VM_POP_CHECK 1
    cmp dx, [edi + STAR_VM_STACK + ebp * 2]
    setc [edi + STAR_VM_T]
    inc ebp
    STAR_VM_END_INSTRUCTION_MEDIUM
%endif
    
    ; 0x18 - TEST
    STAR_VM_START_INSTRUCTION star_vm_test
    ; This is also handwritten because we can squeeze more in if we combine the
    ; arithmetic and the pop.
%ifdef STAR_VM_SLOW
    STAR_VM_POP ax
    test edx, eax
    setz [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    STAR_VM_POP_CHECK 1
    test dx, [edi + STAR_VM_STACK + ebp * 2]
    setz [edi + STAR_VM_T]
    inc ebp
    STAR_VM_END_INSTRUCTION_MEDIUM
%endif
    
    ; 0x19 - BIT
    STAR_VM_START_INSTRUCTION star_vm_bit
    ; This is hand-written since we can combine bounds checks.
%ifdef STAR_VM_SLOW
    STAR_VM_IMM8 ax
    bt edx, eax
    setc [edi + STAR_VM_T]
    STAR_VM_END_INSTRUCTION_SHORT
%else
    inc esi
    cmp esi, ecx
    jge star_vm_fault_
    movzx eax, WORD [ebx + esi - 1]
    bt edx, eax
    setc [edi + STAR_VM_T]
    inc esi
    shr eax, (8 - STAR_VM_INSTRUCTION_SHIFT)
    add eax, star_vm_dispatch_
    jmp eax
    STAR_VM_END_INSTRUCTION
%endif

    ; 0x1A - CT
    STAR_VM_START_INSTRUCTION star_vm_ct
    xor eax, eax
    mov [edi + STAR_VM_T], al
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x1B - ST
    STAR_VM_START_INSTRUCTION star_vm_st
    mov eax, 1
    mov [edi + STAR_VM_T], al
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x1C - CLT
    STAR_VM_START_INSTRUCTION star_vm_clt
    xor BYTE [edi + STAR_VM_T], 1
    STAR_VM_END_INSTRUCTION_LONG
    
    ; 0x1D - JP
    STAR_VM_START_INSTRUCTION star_vm_jp
    ; Handwritten because we can elide the last addition to the PC.
    cmp esi, ecx
    jge star_vm_fault_
.do:
    movzx esi, WORD [ebx + esi]
    ror si, 8
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x1E - JPT
    STAR_VM_START_INSTRUCTION star_vm_jpt
    test BYTE [edi + STAR_VM_T], 1
    jnz star_vm_jp
    add esi, 2
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x1F - JPF
    STAR_VM_START_INSTRUCTION star_vm_jpf
    test BYTE [edi + STAR_VM_T], 1
    jz star_vm_jp
    add esi, 2
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x20 - JR
    STAR_VM_START_INSTRUCTION star_vm_jr
    cmp esi, ecx
    jge star_vm_fault_
    movsx eax, BYTE [ebx + esi]
    lea esi, [esi + eax + 1]
    xor eax, eax ; We require that the high 16-bits of eax are zero.
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x21 - JRT
    STAR_VM_START_INSTRUCTION star_vm_jrt
    test BYTE [edi + STAR_VM_T], 1
    jnz star_vm_jr
    inc esi
    STAR_VM_END_INSTRUCTION_MEDIUM
    
    ; 0x22 - JRT
    STAR_VM_START_INSTRUCTION star_vm_jrf
    test BYTE [edi + STAR_VM_T], 1
    jz star_vm_jr
    inc esi
    STAR_VM_END_INSTRUCTION_MEDIUM

    ; 0x23 - CALL
    STAR_VM_START_INSTRUCTION star_vm_call
    ; Handwritten, because we need to elide the PC add at the end.
    STAR_VM_PUSH si
    cmp esi, ecx
    jl star_vm_jp.do
    jmp star_vm_fault_
    STAR_VM_END_INSTRUCTION
    
    ; 0x24 - RET
    STAR_VM_START_INSTRUCTION star_vm_ret
    STAR_VM_POP si
    jmp star_vm_ret_dispatch
    STAR_VM_END_INSTRUCTION
    ; 0x25 - TRAP
    STAR_VM_START_INSTRUCTION star_vm_trap
    STAR_VM_RETURN STAR_VM_RESULT_TRAP
    STAR_VM_END_INSTRUCTION
    ; 0x26 - STOP
    STAR_VM_START_INSTRUCTION star_vm_stop
    STAR_VM_RETURN STAR_VM_RESULT_OK
    STAR_VM_END_INSTRUCTION
star_vm_ret_dispatch:
    STAR_VM_DISPATCH

