/* Copyright (c) 2023 AlaskanEmily
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *  
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 * 
 *   3. Trans rights are human rights.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

#include "star_vm.h"

/*****************************************************************************/

int Star_Interpret(struct Star_VM *vm, const void *prog, unsigned len){
    const unsigned char *bytes;
    unsigned char tmp8;
    unsigned short tmp16;
    unsigned long tmp32;
    bytes = prog;
#define STACK_POP_CHECK(I) do{ \
    if(vm->sp > STAR_VM_STACK_SIZE - (I)) \
        return STAR_VM_RESULT_STACK_OVERFLOW; \
}while(0)

#define STACK_PUSH_CHECK(I) do{ \
    if(vm->sp < (I)) \
        return STAR_VM_RESULT_STACK_UNDERFLOW; \
}while(0)

#define IMM_CHECK(I) do{ \
    if(vm->pc + (I) - 1u >= len) \
        return STAR_VM_RESULT_FAULT; \
}while(0)

#define IMM8() (bytes[vm->pc++])

#define IMM16() \
    ((vm->pc += 2), \
        ((unsigned)(bytes[vm->pc - 2]) << 8) | (unsigned)(bytes[vm->pc - 1]))

    while(vm->pc < len){
        switch(bytes[vm->pc++]){
            case STAR_VM_NOP:
                break;
            case STAR_VM_PUSHA:
                STACK_PUSH_CHECK(1);
                vm->stack[--(vm->sp)] = vm->a;
                break;
            case STAR_VM_POPA:
                STACK_POP_CHECK(1);
                vm->a = vm->stack[vm->sp++];
                break;
            case STAR_VM_PUSHB:
                STACK_PUSH_CHECK(1);
                vm->stack[--(vm->sp)] = vm->b;
                break;
            case STAR_VM_POPB:
                STACK_POP_CHECK(1);
                vm->b = vm->stack[vm->sp++];
                break;
            case STAR_VM_SWAP:
                STACK_POP_CHECK(2);
                tmp16 = vm->stack[vm->sp];
                vm->stack[vm->sp] = vm->stack[vm->sp + 1];
                vm->stack[vm->sp + 1] = tmp16;
                break;
            case STAR_VM_DUP:
                STACK_POP_CHECK(1);
                STACK_PUSH_CHECK(1);
                tmp16 = vm->stack[vm->sp];
                vm->stack[--(vm->sp)] = tmp16;
                break;
            case STAR_VM_ZERO:
                vm->a = 0;
                break;
            case STAR_VM_ONE:
                vm->a = 1;
                break;
            case STAR_VM_SET:
                IMM_CHECK(2);
                vm->a = IMM16();
                break;
            case STAR_VM_ADD:
                STACK_POP_CHECK(1);
                tmp32 = vm->stack[vm->sp++];
                tmp32 += vm->a;
                vm->t = tmp32 > 0xFFFF;
                vm->a = tmp32;
                break;
            case STAR_VM_SUB:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = vm->a < tmp16;
                vm->a -= tmp16;
                break;
            case STAR_VM_MUL:
                STACK_POP_CHECK(1);
                tmp32 = vm->stack[vm->sp++];
                tmp32 *= vm->a;
                vm->t = tmp32 > 0xFFFF;
                vm->a = tmp32;
                break;
            case STAR_VM_DIV:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = vm->a % tmp16 != 0;
                vm->a /= tmp16;
                break;
            case STAR_VM_REM:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = vm->a >= tmp16;
                vm->a %= tmp16;
                break;
            case STAR_VM_INC:
                vm->t = ++(vm->a) == 0;
                break;
            case STAR_VM_DEC:
                vm->t = (vm->a--) == 0;
                break;
            case STAR_VM_AND:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = (vm->a &= tmp16) == 0;
                break;
            case STAR_VM_OR:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = (vm->a |= tmp16) == 0;
                break;
            case STAR_VM_XOR:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = (vm->a ^= tmp16) == 0;
                break;
            case STAR_VM_NOT:
                vm->a = ~vm->a;
                vm->t = vm->a == 0;
                break;
            case STAR_VM_SHL:
                vm->t = (vm->a & 0x8000) != 0;
                vm->a <<= 1;
                break;
            case STAR_VM_SHR:
                vm->t = vm->a & 1;
                vm->a >>= 1;
                vm->a &= 0x7FFF;
                break;
            case STAR_VM_CMP:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = vm->a < tmp16;
                break;
            case STAR_VM_TEST:
                STACK_POP_CHECK(1);
                tmp16 = vm->stack[vm->sp++];
                vm->t = (vm->a & tmp16) == 0;
                break;
            case STAR_VM_BIT:
                IMM_CHECK(1);
                tmp8 = IMM8();
                vm->t = !!(vm->a & (1 << tmp8));
                break;
            case STAR_VM_CT:
                vm->t = 0;
                break;
            case STAR_VM_ST:
                vm->t = 1;
                break;
            case STAR_VM_CLT:
                vm->t ^= 1;
                break;
            case STAR_VM_JP:
                IMM_CHECK(2);
                vm->pc = IMM16();
                break;
            case STAR_VM_JPT:
                IMM_CHECK(2);
                tmp16 = IMM16();
                if(vm->t)
                    vm->pc = tmp16;
                break;
            case STAR_VM_JPF:
                IMM_CHECK(2);
                tmp16 = IMM16();
                if(vm->t)
                    vm->pc = tmp16;
                break;
            case STAR_VM_JR:
                IMM_CHECK(1);
                tmp8 = IMM8();
                vm->pc += (signed char)tmp8;
                break;
            case STAR_VM_JRT:
                IMM_CHECK(1);
                tmp8 = IMM8();
                if(vm->t)
                    vm->pc += (signed char)tmp8;
                break;
            case STAR_VM_JRF:
                IMM_CHECK(1);
                tmp8 = IMM8();
                if(!vm->t)
                    vm->pc += (signed char)tmp8;
                break;
            case STAR_VM_CALL:
                IMM_CHECK(2);
                STACK_PUSH_CHECK(1);
                tmp16 = IMM16();
                vm->stack[--(vm->sp)] = vm->pc;
                vm->pc = tmp16;
                break;
            case STAR_VM_RET:
                STACK_POP_CHECK(1);
                vm->pc = vm->stack[vm->sp++];
                break;
            case STAR_VM_TRAP:
                return STAR_VM_RESULT_TRAP;
            case STAR_VM_STOP:
                return STAR_VM_RESULT_OK;
        }
    }
    return STAR_VM_RESULT_FAULT;
}

/*****************************************************************************/

