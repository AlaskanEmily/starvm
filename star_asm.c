/* Copyright (c) 2023 AlaskanEmily
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *  
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 * 
 *   3. Trans rights are human rights.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include "star_asm.h"

#include "y.tab.h"

#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#if (defined _WIN32) || (defined __WATCOMC__) || (defined __BORLANDC__)
# define strcasecmp _stricmp
#else
# include <strings.h>
#endif

/*****************************************************************************/

#define relative 1
#define absolute 2

struct opcode{
    char mnemonic[7];
    unsigned char arg;
};

/*****************************************************************************/

static const struct opcode opcodes[] = {
    {"nop", 0 },
    {"pusha", 0 },
    {"popa", 0 },
    {"pushb", 0 },
    {"popb", 0 },
    {"swap", 0 },
    {"dup", 0 },
    {"zero", 0 },
    {"one", 0 },
    {"set", absolute },
    {"add", 0 },
    {"sub", 0 },
    {"mul", 0 },
    {"div", 0 },
    {"rem", 0 },
    {"inc", 0 },
    {"dec", 0 },
    {"and", 0 },
    {"or", 0 },
    {"xor", 0 },
    {"not", 0 },
    {"shl", 0 },
    {"shr", 0 },
    {"cmp", 0 },
    {"test", 0 },
    {"bit", 0 },
    {"ct", 0 },
    {"st", 0 },
    {"clt", 0 },
    {"jp", absolute },
    {"jpt", absolute },
    {"jpf", absolute },
    {"jr", relative },
    {"jrt", relative },
    {"jrf", 0 },
    {"call", absolute },
    {"ret", 0 },
    {"trap", 0 },
    {"stop", 0 }
};

/*****************************************************************************/

int star_line;
int star_error;

/*****************************************************************************/

struct label{
    char *l;
    int line;
    unsigned short code;
};

static struct label *labels = NULL;
static unsigned num_labels;
static unsigned cap_labels = 0;

struct reloc{
    char *l;
    int line;
    unsigned short code;
    unsigned char type;
};
static struct reloc *relocs = NULL;
static unsigned num_relocs;
static unsigned cap_relocs = 0;

static unsigned char *code = NULL;
static unsigned code_len;
static unsigned cap_code = 0;

/*****************************************************************************/

int Star_ASM_CheckOpcode(const char *text){
    unsigned i;
    for(i = 0; i < sizeof(opcodes) / sizeof(*opcodes); i++){
        if(strcasecmp(text, opcodes[i].mnemonic) == 0)
            return i;
    }
    return -1;
}

/*****************************************************************************/

void Star_ASM_Init(void){
    star_line = 1;
    star_error = 0;
    num_labels = 0;
    num_relocs = 0;
    code_len = 0;
}

/*****************************************************************************/

#define RESERVE_LABELS() do{ \
    if(num_labels == cap_labels){ \
        cap_labels += 16; \
        labels = realloc(labels, cap_labels * sizeof(*labels)); \
        if(labels == NULL){ \
            fputs("OUT OF MEMORY\n", stderr); \
            abort(); \
        } \
    } \
}while(0)

#define RESERVE_CODE() do{ \
    if(code_len + 8 >= cap_code){ \
        if(cap_code == 0) \
            cap_code = 256; \
        else \
            cap_code <<= 1; \
        code = realloc(code, cap_code * sizeof(*code)); \
        if(code == NULL){ \
            fputs("OUT OF MEMORY\n", stderr); \
            abort(); \
        } \
    } \
}while(0)

#define RESERVE_RELOCS() do{ \
    if(cap_relocs == num_relocs){ \
        if(cap_relocs == 0) \
            cap_relocs = 64; \
        else \
            cap_relocs <<= 1; \
        relocs = realloc(relocs, cap_relocs * sizeof(*relocs)); \
        if(relocs == NULL){ \
            fputs("OUT OF MEMORY\n", stderr); \
            abort(); \
        } \
    } \
}while(0)

/*****************************************************************************/

void Star_ASM_Emit(int o, ...){
    va_list args;
    int ty, i;
    va_start(args, o);
    ty = va_arg(args, int);
    if((opcodes[o].arg == 0) != (ty == 0)){
        fprintf(
            stderr,
            "ERROR Line %i: Invalid number of arguments for opcode %s\n",
            star_line,
            opcodes[o].mnemonic);
        star_error = 1;
        return;
    }
    RESERVE_CODE();
    code[code_len++] = o;
    if(ty == INTEGER){
        i = va_arg(args, int);
        if(opcodes[o].arg == relative){
            if(i > 127 || i < -128){
                fprintf(
                    stderr,
                    "WARNING Line %i: Relative symbol out of range\n",
                    star_line);
            }
            ((signed char*)code)[code_len] = (signed char)i;
        }
        else{
            ((signed char*)code)[code_len] = (i >> 8) & 0xFF;
            ((signed char*)code)[code_len + 1] = i & 0xFF;
        }
    }
    else if(ty == LABEL){
        /* Lay down the reloc. */
        RESERVE_RELOCS();
        relocs[num_relocs].l = strdup(va_arg(args, char*));
        relocs[num_relocs].line = star_line;
        relocs[num_relocs].type = opcodes[o].arg;
        relocs[num_relocs].code = code_len;
        num_relocs++;
    }
    code_len += opcodes[o].arg;
}

/*****************************************************************************/

void Star_ASM_Label(char *l){
    unsigned i;
    for(i = 0; i < num_labels; i++){
        if(strcmp(labels[i].l, l) == 0){
            fprintf(
                stderr,
                "WARNING Line %i: label %s repeated, previously on line %i\n",
                star_line,
                l,
                labels[i].line);
            return;
        }
    }
    RESERVE_LABELS();
    labels[num_labels].l = strdup(l);
    labels[num_labels].line = star_line;
    labels[num_labels].code = code_len;
    num_labels++;
}

/*****************************************************************************/

int Star_ASM_Write(FILE *f){
    unsigned i, e;
    long ln;
    
    if(code_len > 0xFFFF){
        fprintf(
            stderr,
            "ERROR Line %i: code is size %i, max is %i\n",
                star_line,
                code_len,
                0xFFFF);
        star_error = 1;
    }
    
    /* Fixup relocs. */
    for(i = 0; i < num_relocs; i++){
        for(e = 0; e < num_labels; e++){
            if(strcmp(relocs[i].l, labels[e].l) == 0){
                if(relocs[i].type == relative){
                    ln = labels[e].code - (relocs[i].code + 1);
                    if(ln < -128 || ln > 127){
                        fprintf(
                            stderr,
                            "WARNING Line %i: Relative symbol out of range\n",
                            star_line);
                    }
                    ((signed char*)code)[relocs[i].code] = (signed char)ln;
                }
                else{
                    code[relocs[i].code] = (labels[e].code >> 8) & 0xFF;
                    code[relocs[i].code + 1] = labels[e].code & 0xFF;
                }
                break;
            }
        }
        if(e == num_labels){
            fprintf(
                stderr,
                "ERROR Line %i: symbol %s referenced but not found\n",
                relocs[i].line,
                relocs[i].l);
            star_error = 1;
        }
    }
    
    if(star_error)
        return 1;
    
    if(fwrite(code, 1, code_len, f) != code_len){
        fputs("IO ERROR\n", stderr);
        return 1;
    }
    
    return 0;
}

/*****************************************************************************/

void Star_ASM_Error(const char *s){
    fprintf(
        stderr,
        "ERROR Line %i: %s\n",
        star_line,
        s);
    star_error = 1;
        
}

/*****************************************************************************/

